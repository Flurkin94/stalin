-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script

local twitch = nil
local twitch2 = nil
local newState = { }

set = 1
local point = 1
local held = false
lvl = 0
dialogue = {
    -- Introductory story (lvl 0)
    {
        'The year is 1945.',
        'Germany has been defeated by the great and powerful Joseph Stalin.',
        'But what would he do now that his greatest enemy had been defeated.',
        'So he searched',
        'Searched for what seemed like an eternity.',
        'Until...',
    },

    {
        'Stalin: Well I have single handedly stopped Hitler...',
        'Stalin: What do I do now??',
        'God: You must cleanse the Earth!',
        'Stalin: Who are you?',
        'God: I am Communist God!',
        'Stalin: ...',
        'God: Venture forth and you will find the way.',
    },

    -- Level 1, Russia
    {
        'Stalin began the cleanse with his own people.',
        'Although many people said he was a mad man,',
        'he knew he was doing the right thing.',
        'He knew what needed to be done...',
    },

    {
        'Time for Stalin to tear shit up!',
    },

    -- Level 2, Return of the Nazis
    {
        'Word had spread of the cleansing.',
        'It was as if the whole world had gone into hiding.',
        'But there was one group still around.',
        'It was the Nazis.',
        'The Nazis wanted revenge.',
    },

    {
        'Stalin: If these Nazi bastards want a fight, thats what theyll get!',
        'Stalin: Time to DIE!',
    },

    -- Level 3, Someone
    {
        '',
        '',
        '',
        '',
    },

    {
        '',
        '',
        '',
        '',
    },

    -- Level 4, Boss Fight
    {
        '',
        '',
        '',
        '',
    },

    {
        '',
        '',
        '',
        '',
    },

}



-- On state create
function newState:create()

    held = true
    a.stop()
    a.play(bgm.transition)

end


-- On state update
function newState:update(dt)

    if k.isDown(' ') and not held then
        held = true
        point = point + 1
        if point > #dialogue[(set + lvl * 2)] then
            point = 1
            set = set + 1
            if set >= 3 then
                set = 1
                state.set(states.play)
            else
                state.load(states.play)
            end
        end
    elseif not k.isDown(' ') and held then
        held = false
    end

end


-- On state draw
function newState:draw()

    g.setFont(fnt.regular)

    if set == 2 then
        states.play.draw()
        g.setColor(0, 0, 0)
        g.rectangle('fill', 0, 0, 854, 80)

        g.setColor(255, 255, 255)
        g.rectangle('line', 0, 0, 854, 80)
        g.print(dialogue[set + lvl * 2][point])

    else
        g.setBackgroundColor(0, 0, 0)
        g.setColor(255, 255, 255)
        for i = 1, #dialogue[set + lvl * 2] do
            if i <= point then
                g.printf(dialogue[set + lvl * 2][i], 10, - 70 + i * 80, 834, 'center')
            end
        end
    end

end


-- On state kill
function newState:kill()

    a.stop()

    if lvl == 0 then
        twitch = 'transition'
    else
        twitch = nil
    end
    if lvl == 4 then
        twitch2 = 'Boss'
    else
        twitch2 = nil
    end
    a.play(bgm[twitch or ('stalin' .. (twitch2 or lvl))])

end


-- Transfer data to state loading script
return newState
