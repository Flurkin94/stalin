return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.11.0",
  orientation = "orthogonal",
  width = 1,
  height = 1,
  tilewidth = 854,
  tileheight = 480,
  nextobjectid = 2,
  properties = {},
  tilesets = {
    {
      name = "level0",
      firstgid = 1,
      tilewidth = 854,
      tileheight = 480,
      spacing = 0,
      margin = 0,
      image = "../gfx/level0.png",
      imagewidth = 854,
      imageheight = 480,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tiles = {}
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "Tile Layer 1",
      x = 0,
      y = 0,
      width = 1,
      height = 1,
      visible = true,
      opacity = 1,
      properties = {},
      encoding = "lua",
      data = {
        1
      }
    },
    {
      type = "objectgroup",
      name = "collisions",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {}
    },
    {
      type = "objectgroup",
      name = "important",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {
        {
          id = 1,
          name = "spawn",
          type = "",
          shape = "rectangle",
          x = 233,
          y = 242,
          width = 85,
          height = 150,
          rotation = 0,
          visible = true,
          properties = {}
        }
      }
    },
    {
      type = "objectgroup",
      name = "enemies",
      visible = true,
      opacity = 1,
      properties = {},
      objects = {}
    }
  }
}
