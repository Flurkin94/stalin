-- chikun :: 2015
-- Player script



player = {}
local imageTimer = 0





-- Called when map loads
player.set = function(x, y, w, h)


    -- Set player position
    player.x = x ; player.y = y
    player.w = w ; player.h = h

    -- Set player image
    player.image = gfx.player

    -- Set player score
    player.score = 0

    -- Combat variables
    player.direction = 'right'
    player.blockObject = { }
    player.attack = nil
    player.attackHeld = false
    player.attackTimer = 0
    player.attackTimerDelay = 0
    player.arm = { }
    player.hp = 10
    player.maxHp = player.hp

    -- Graphical variables
    player.image = gfx.standingStalin
    player.imageTable = {
        gfx.walkingStalin1,
        gfx.standingStalin,
        gfx.walkingStalin2,
        gfx.standingStalin
    }
    player.imagePunch = {
        gfx.topPunchStalin,
        gfx.bottomPunchStalin
    }
    player.punch = 0


end






-- Called using update phase
player.update = function(dt)


    -- Figure out which direction the player will move horizontally
    local xMove = 0
    if (k.isDown('left') or k.isDown('a'))  then
        xMove = -1
    end
    if (k.isDown('right') or k.isDown'd') then
        xMove = xMove + 1
    end

    if xMove == -1 then
        player.direction = 'left'
        imageTimer = imageTimer + dt
        if imageTimer > 0.8 then
            imageTimer = imageTimer - 0.8
        end
        player.image = player.imageTable[math.ceil(imageTimer * 5)]
    elseif xMove == 1 then
        player.direction = 'right'
        imageTimer = imageTimer + dt
        if imageTimer > 0.8 then
            imageTimer = imageTimer - 0.8
        end
        player.image = player.imageTable[math.ceil(imageTimer * 5)]
    elseif xMove == 0 then
        player.image = gfx.standingStalin
    end
    -- Actually move the player horizontal
    player.x = player.x + 256 * dt * xMove

    -- Prevent running where you can't
    if not math.overlapTable(collisions, player) then
        player.x = player.x - 256 * dt * xMove
    end



    local yMove = 0
    if (k.isDown('up') or k.isDown('w')) then
        yMove = - 1
        if xMove == 0 then
            imageTimer = imageTimer + dt
            if imageTimer > 0.8 then
                imageTimer = imageTimer - 0.8
            end
        end
        player.image= player.imageTable[math.ceil(imageTimer * 5)]
    end
    if (k.isDown('down') or k.isDown('s')) then
        yMove = yMove + 1
        if xMove == 0 then
            imageTimer = imageTimer + dt
            if imageTimer > 0.8 then
                imageTimer = imageTimer - 0.8
            end
        end
        player.image= player.imageTable[math.ceil(imageTimer * 5)]
    end

    player.y = player.y + 256 * dt * yMove

    if not math.overlapTable(collisions, player) then
        player.y = player.y - 256 * dt * yMove
    end


    -- local variable to account for direction
    local side = 0
    local adjust = 0

    -- change side to match which way the player is facing
    if player.direction == "right" then
        side = player.w
    else
        adjust = player.w / 8
    end


    -- Prevent player spamming attacks constantly or attcking when changing blocking
    if player.attackTimerDelay == 0 and player.attack == nil and not k.isDown('q') then

        player.block = false
        player.blockObject = {
            x = 0, y = 0, w = 0, h = 0
        }
        -- only attack
        if k.isDown('e') and xMove == 0 and not player.attackHeld then
            player.attackHeld = true
            player.attack = 'mid'
            player.attackTimer = 0.1
            player.attackTimerDelay = 0.5
            player.image = player.imagePunch[player.punch + 1]
            player.arm = {
                x = player.x + side,
                y = player.y,
                w = 0,
                h = player.h / 4
            }
            a.play(sfx.hoo)
        elseif not k.isDown('e') then
            player.attackHeld = false
        end

    -- set block
    elseif k.isDown('q') then
        -- .. middle ..
        player.block = true
        player.blockObject = {
            x = player.x + side - adjust,
            y = player.y + player.h / 3,
            w = player.w / 8,
            h = player.h / 3
        }

    end

    -- Move the block object to stay in the relative position of the player
    player.blockObject.x = player.x + side - adjust
    if player.block == true then
        player.blockObject.y = player.y + player.h / 3
    end

    -- Kill the arm when finished
    if player.attackTimer == 0 and player.attack ~= nil then
        player.punch = math.abs(player.punch - 1)
        player.attack = nil
        player.arm = { }
    end

    -- count down
    if player.attackTimer > 0 then
        player.image = player.imagePunch[player.punch + 1]
        player.attackTimer = player.attackTimer - dt
        if player.attackTimer <= 0 then
            player.attackTimer = 0
        end

        -- make sure arm is on the correct side
        player.arm.x = player.x + side
        -- make sure the arm is at the right height
        if player.attack == 'mid' then
            player.arm.y = player.y + player.h / 2 - player.arm.h / 2
        end
        -- change width of player arm
        player.arm.w = player.w / 4 - player.attackTimer * player.w / 4 * 10
        -- make sure player arm move away if facing left
        if player.direction == 'left' then
            player.arm.x = player.x - player.arm.w
        end
    end

    if player.attackTimerDelay > 0 then
        player.attackTimerDelay = player.attackTimerDelay - dt
        if player.attackTimerDelay <= 0 then
            player.attackTimerDelay = 0
        end
    end

    if player.hp <= 0 then
        state.change(states.death)
    end

end








-- Called using draw phase
player.draw = function()


    local playerDir = 1

    if player.direction == "left" then

        playerDir = -1

    end

    -- Draw player
    g.draw(player.image, player.x + player.w / 2, player.y, 0, playerDir, 1,
        player.w / 2, 0)

    -- Draw player healthbar
    g.setColor(255, 0, 0)
    g.rectangle('fill', player.x + player.w / 2 - 55, player.y - 40, player.maxHp * 10 + 10, 20)
    g.setColor(0, 255, 0)
    g.rectangle('fill', player.x + player.w / 2 - 50, player.y - 35, player.hp * 10, 10)


end








-- Check vertical movement
player.checkVertical = function(dt)


    -- Can the player jump?
    local canJump = false

    -- Check if top half of player colliding
    topHalf = {
        x = player.x,
        y = player.y,
        w = player.w,
        h = player.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, topHalf)) do

        -- Move box out of collision
        topHalf.y = math.floor(topHalf.y + 1)

        -- Set player's ySpeed to 0
        player.ySpeed = 0

    end

    -- Move player to determined y position
    player.y = topHalf.y


    -- Check if bottom half of player colliding
    bottomHalf = {
        x = player.x,
        y = player.y + player.h / 2,
        w = player.w,
        h = player.h / 2
    }

    -- While colliding...
    while (math.overlapTable(collisions, bottomHalf)) do

        -- Move box out of collision
        bottomHalf.y = math.ceil(bottomHalf.y - 1)

        -- Set player's ySpeed to 0
        player.ySpeed = 0

        -- Allow player to jump
        canJump = true

    end

    -- Move player to determined y position
    player.y = bottomHalf.y - player.h / 2

    -- Make player jump if holding spacebar and can
    if (canJump and (k.isDown('up') or k.isDown('w'))) then

        -- Up they go
        player.ySpeed = -768

    end


end



-- Check horizontal movement
player.checkHorizontal = function(dt)


    -- Check if left half of player colliding
    leftHalf = {
        x = player.x,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, leftHalf)) do

        -- Move box out of collision
        leftHalf.x = math.floor(leftHalf.x + 1)

    end

    -- Move player to determined x position
    player.x = leftHalf.x

    -- Check if right half of player colliding
    rightHalf = {
        x = player.x + player.w / 2,
        y = player.y,
        w = player.w / 2,
        h = player.h
    }

    -- While colliding...
    while (math.overlapTable(collisions, rightHalf)) do

        -- Move box out of collision
        rightHalf.x = math.floor(rightHalf.x - 1)

    end

    -- Move player to determined x position
    player.x = rightHalf.x - player.w / 2


end
