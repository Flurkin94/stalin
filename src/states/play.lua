-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }
view = {
    x = 0,
    y = 0,
    w = 854,
    h = 480
}

local twitch = nil
local twitch2 = nil

-- On state create
function newState:create()

    if lvl == 0 then
        twitch = 'transition'
    else
        twitch = nil
    end
    if lvl == 4 then
        twitch2 = 'Boss'
    else
        twitch2 = nil
    end


    if set == 1 then
        a.play(bgm[twitch or('stalin' .. (twitch2 or lvl))])
    end
    map.current = maps['level' .. (lvl)]

    -- load up the oobjects on map
    for key,layer in ipairs(map.current.layers) do

        -- load up all collidable objects
        if layer.name == 'collisions' then
            collisions = layer.objects
        end

        -- load up all important objects
        if layer.name == 'important' then

            for key,object in ipairs(layer.objects) do

                -- create the player
                if object.name == 'spawn' then
                    player.set(object.x, object.y, object.w, object.h)
                end

            end

        end

        if layer.name == 'enemies' then
            for key, object in ipairs(layer.objects) do
                if lvl == 1 then
                    cilivian.create(object.x, object.y, object.w, object.h)
                elseif lvl == 2 then
                    enemy.create(object.x, object.y, object.w, object.h)
                end
            end
        end

    end

end


-- On state update
function newState:update(dt)

    if set == 2 then
        state.current = states.dialogue
    elseif lvl == 0 then
        lvl = lvl + 1
        state.change(states.dialogue)
    elseif lvl == 1 and #cilivians == 0 then
        lvl = lvl + 1
        state.change(states.dialogue)
    elseif lvl == 2 and #enemies == 0 then
        lvl = lvl + 1
        state.change(states.dialogue)
    end

    -- view moves based on where the player is
    view.x = math.clamp(0, (player.x or 0) - 854/2 + (player.w or 0), map.current.w * 34 - view.w)
    view.y = math.clamp(0, (player.y or 0) - 480/2 + (player.h or 0), map.current.h * 34 - view.h)

    -- update where the player is
    player.update(dt)

    if lvl == 1 then
        cilivian.update(dt)
    elseif lvl == 2 then
        enemy.update(dt)
    end

    bullet.update(dt)

end


-- On state draw
function newState:draw()

    -- set draw color to white
    g.setColor(255, 255, 255)

    -- move the drawing in accordace to the view
    g.translate(-view.x, -view.y)

    -- draw the map
    map.draw()

    -- draw the player
    player.draw()
    g.setColor(255, 0, 0)
    g.rectangle('fill', player.arm.x or 0, player.arm.y or 0,
                        player.arm.w or 0, player.arm.h or 0)
    g.setColor(0, 0, 255)
    g.rectangle('fill', player.blockObject.x or 0, player.blockObject.y or 0,
                        player.blockObject.w or 0, player.blockObject.h or 0)

    g.setColor(0, 255, 126)
    enemy.draw()

    g.setColor(255, 0, 0)
    bullet.draw()

    cilivian.draw()

end


-- On state kill
function newState:kill()

    a.stop()
    a.play(bgm.transition)

end


-- Transfer data to state loading script
return newState
