-- chikun :: 2015
-- First level enemies


cilivian = { }
cilivians = { }

function cilivian.create (X, Y, W, H)
    local gender = math.random(2)
    cilivians[#cilivians + 1] = {
        x = X,
        y = Y,
        w = W,
        h = H,
        imageTable = {
            gfx['cilivian' .. gender .. '_1'],
            gfx['cilivian' .. gender .. '_2']
        },
        image = gfx['cilivian' .. gender .. '_1'],
        imageTimer = 0,
        direction = 1
    }

end

function cilivian.update(dt)
    for key, object in ipairs(cilivians) do
        if math.overlap(object, view) then
            if object.x > player.x then
                object.x = object.x + 128 * dt
                object.direction = 1
                if not math.overlapTable(collisions, object) then
                    object.x = object.x - 128 * dt
                end
                object.imageTimer = object.imageTimer + dt
                if object.imageTimer > 2 then
                    object.imageTimer = object.imageTimer - 2
                end
                object.image = object.imageTable[math.ceil(object.imageTimer)]
            elseif object.x < player.x then
                object.x = object.x - 128 * dt
                object.direction = - 1
                if not math.overlapTable(collisions, object) then
                    object.x = object.x + 128 * dt
                end
                object.imageTimer = object.imageTimer + dt
                if object.imageTimer > 2 then
                    object.imageTimer = object.imageTimer - 2
                end
                object.image = object.imageTable[math.ceil(object.imageTimer)]
            end

            if object.y > player.y then
                object.y = object.y + 128 * dt
                if not math.overlapTable(collisions, object) then
                    object.y = object.y - 128 * dt
                end
                object.imageTimer = object.imageTimer + dt
                if object.imageTimer > 2 then
                    object.imageTimer = object.imageTimer - 2
                end
                object.image = object.imageTable[math.ceil(object.imageTimer)]
            elseif object.y < player.y then
                object.y = object.y - 128 * dt
                if math.overlapTable(collisions, object) then
                    object.y = object.y + 128 * dt
                end
                object.imageTimer = object.imageTimer + dt
                if object.imageTimer > 2 then
                    object.imageTimer = object.imageTimer - 2
                end
                object.image = object.imageTable[math.ceil(object.imageTimer)]
            end

            if player.attack ~= nil then
                if math.overlap(player.arm, object) then
                    a.play(sfx.hit)
                    table.remove(cilivians, key)
                end
            end
        end
    end
end


function cilivian.draw()

    g.setColor(255, 255, 255)

    for key, object in ipairs(cilivians) do

        g.draw(object.image, object.x + object.w / 2, object.y, 0, object.direction, 1, object.w / 2, 0)

    end

end
