-- chikun :: 2015
-- Enemies script

enemy   = { }
enemies = { }

local weirdiness = 1
local moveTable = {
    gfx.knifeEnemy,
    gfx.knifeEnemy3,
    gfx.knifeEnemy2,
    gfx.knifeEnemy3
}
local attackTable = {
    gfx.knifeEnemy3,
    gfx.knifeEnemy4
}

local attackSpeed = 0

function enemy.create(tempX, tempY, tempW, tempH)
    local tempImage = nil
    if lvl == 2 then
        tempImage = gfx.shootingEnemy
    end

    print(tempW, tempH)

    enemies[#enemies + 1] = {
        x = tempX,
        xMove = 0,
        y = tempY,
        yMove = 0,
        w = tempW,
        h = tempH,
        hp = 3,
        immuneTimer = 0,
        image = tempImage,
        imagePos = nil
    }

end


function enemy.update(dt)

    for key, object in ipairs(enemies) do

        if math.overlap(object, view) then
            local xMove = false
            local yMove = false

            if object.imagePos then
                local dir = 0
                local swap = 1
                if object.immuneTimer > 0 then
                    swap = - 1
                end

                if object.x + object.w + 10 < player.x then

                    xMove = true
                    object.x = object.x + 256 * dt * swap

                    object.imagePos = object.imagePos + dt * 4
                    if object.imagePos > 4 then
                        object.imagePos = object.imagePos - 4
                    end
                    object.image = moveTable[math.ceil(object.imagePos)]

                elseif object.x < player.x and object.x + object.w + 5 > player.x then

                    xMove = true

                    object.x = object.x - 256 * dt * swap

                    object.imagePos = object.imagePos + dt * 4
                    if object.imagePos > 4 then
                        object.imagePos = object.imagePos - 4
                    end
                    object.image = moveTable[math.ceil(object.imagePos)]

                elseif object.x > player.x and object.x - 5 < player.x + player.w then

                    xMove = true

                    object.x = object.x + 256 * dt * swap

                    object.imagePos = object.imagePos + dt * 4
                    if object.imagePos > 4 then
                        object.imagePos = object.imagePos - 4
                    end
                    object.image = moveTable[math.ceil(object.imagePos)]

                elseif object.x - 10 > player.x + player.w then

                    xMove = true

                    object.x = object.x - 256 * dt * swap

                    object.imagePos = object.imagePos + dt * 4
                    if object.imagePos > 4 then
                        object.imagePos = object.imagePos - 4
                    end
                    object.image = moveTable[math.ceil(object.imagePos)]

                end -- note to self

                if object.y + object.h < player.y + player.h / 2 then

                    yMove = true

                    object.y = object.y + 256 * dt * swap

                    if not xMove then
                        object.imagePos = object.imagePos + dt * 4
                        if object.imagePos > 4 then
                            object.imagePos = object.imagePos - 4
                        end
                    end

                elseif object.y > player.y + player.h / 2 then

                    yMove = true

                    object.y = object.y - 256 * dt * swap

                    if not xMove then
                        object.imagePos = object.imagePos + dt * 4
                        if object.imagePos > 4 then
                            object.imagePos = object.imagePos - 4
                        end
                    end

                end

                if not xMove and not yMove then

                    attackSpeed = attackSpeed + dt
                    if attackSpeed > 0.5 then
                        object.image = attackTable[2]
                        if attackSpeed > 0.75 then
                            attackSpeed = attackSpeed - 0.75
                            if not player.block then
                                player.hp = player.hp - 1
                                a.play(sfx.ooh)
                            end
                        end
                    else
                        object.image = attackTable[1]
                    end

                end



            else
                if math.random(200 + weirdiness) > 200 - weirdiness then

                    bullet.create(object.x, object.y, object.h)
                    sfx.shot:setVolume(0.25)
                    a.play(sfx.shot)
                    weirdiness = 1
                else
                    weirdiness = weirdiness + dt * dt * dt
                end


            end

            print(#bullets)

        end

        if object.immuneTimer ~= 0 then
            object.immuneTimer = object.immuneTimer - dt
            if object.immuneTimer <= 0 then
                object.immuneTimer = 0
            end
        end

        if player.attack ~= nil then
            if math.overlap(object, player.arm) and object.immuneTimer == 0 then

                object.hp = object.hp - 1
                a.play(sfx.hit)

                if object.hp == 0 then
                    print(object.image)
                    table.remove(enemies, key)
                end

                object.immuneTimer = 0.4

                if object.x > player.x then
                    object.x = object.x + 10
                else
                    object.x = object.x - 10
                end

            end

        end

        if object.image == gfx.shootingEnemy then

            if math.abs(object.x - (player.x + player. w / 2)) <= 150 then
                object.image = gfx.knifeEnemy3
                object.imagePos = 4
            end

        end

    end

end


function enemy.draw()

    for key, object in ipairs(enemies) do

        if object.image == nil then
            g.rectangle('fill', object.x, object.y, object.w, object.h)
        else
            g.setColor(255, 255, 255)
            g.draw(object.image, object.x, object.y)
        end

    end

end
