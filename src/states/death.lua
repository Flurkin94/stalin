-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }


-- On state create
function newState:create()
    a.play(sfx.dead)
end


-- On state update
function newState:update(dt)

    if k.isDown(' ') then
        state.change(states.menu)
    end

end


-- On state draw
function newState:draw()

    g.setColor(255, 255, 255)
    g.draw(gfx.death, 0, 0)

end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
