-- chikun :: 2014
-- Template state


-- Temporary state, removed at end of script
local newState = { }

local option = 1
local delay = 0

menu = {
    'start',
    'exit'
}


-- On state create
function newState:create()

    option = 1
    delay = 0.2
    lvl = 0
    set = 1

end


-- On state update
function newState:update(dt)

    if (k.isDown('up') or k.isDown('w')) and delay == 0 then
        option = option - 1
        delay = 0.2
        if option <= 0 then
            option = #menu
        end
    end

    if (k.isDown('down') or k.isDown('s')) and delay == 0 then
        option = option + 1
        delay = 0.2
        if option > #menu then
            option = 1
        end
    end

    if (k.isDown(' ')) and delay == 0 then
        if option == 1 then
            state.change(states.dialogue)
        elseif option == 2 then
            e.quit()
        end
    end

    if delay > 0 then
        delay = delay - dt
        if delay <= 0 then
            delay = 0
        end
    end

end


-- On state draw
function newState:draw()

    g.setColor(255, 255, 255)
    g.draw(gfx.title, 0, 0)

    g.setColor(255, 255, 0)
    if option == 1 then

        g.rectangle('line', 330, 200, 200, 100)

    elseif option == 2 then

        g.rectangle('line', 375, 360, 105, 60)

    end


end


-- On state kill
function newState:kill()

end


-- Transfer data to state loading script
return newState
