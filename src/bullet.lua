-- chikun :: 2015
-- Bullet functions and objects


bullet = { }
bullets = { }

function bullet.create(X, Y, H, dt, delay)

    if delay then
        delay = delay - dt
        if delay <= 0 then
            dlay = nil
        end
    else
        bullets[#bullets + 1] = {
            x = X,
            y = Y + H / 2,
            w = 5,
            h = 5,
            tX = player.x + player.w / 2,
            tY = player.y + player.h / 2,
            speed = 256,
            cos = (X - (player.x + player.w / 2)) / math.dist(player.x + player.w / 2, player.y + player.h / 2, X, Y + H / 2),
            sin = ((Y + H / 2) - (player.y + player.h / 2)) / math.dist(player.x + player.w / 2, player.y + player.h / 2, X, Y + H / 2)
        }

    end

end


function bullet.update(dt)

    for key, object in ipairs(bullets) do

        local xSpeed = object.cos * object.speed
        local ySpeed = object.sin * object.speed

        object.x = object.x - xSpeed * dt
        object.y = object.y - ySpeed * dt

        if not math.overlap(object, view) then
            table.remove(bullets, key)
        end

        if math.overlap(object, player.blockObject) then
            table.remove(bullets, key)
        end

        if math.overlap(object, player) then
            player.hp = player.hp - 1
            a.play(sfx.ooh)
            table.remove(bullets, key)
        end

    end

end


function bullet.draw()

    g.setColor(255, 0, 0)

    for key, object in ipairs(bullets) do

        g.rectangle('fill', object.x, object.y, object.w, object.h)

    end

end
